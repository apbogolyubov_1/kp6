#include <vector>
#include <list>
#include <string>
#include "math.h"
#include <iostream>

using namespace std;

template <typename Key, typename Value>
class HashTable {
    unsigned capacity = 100;
    unsigned size = 0;
    vector<list<pair<Key, Value>>> table;

public:
    HashTable () {
        table = vector<list<pair<Key, Value>>>(100);
    }
    ~HashTable() {
        table.clear();
    }
    void Insert(const Key& key, const Value & val){
        size_t pos = hash_func(key);
        pair <Key, Value> pair (key, val);
        table[pos].push_back(pair);
    }
    size_t hash_func (const string & key) {
        unsigned int p = 127;
        int M = 97;
        unsigned long hash = 0;
        for (int i = 0; i < key.size(); i++) {
            hash += key[i]*(unsigned long)pow(p,i) % M;
        }
        hash = hash % M;
        return static_cast <size_t> (hash);
    }
    void Output () {
        for (auto list : table) {
            for (auto elem : list) {
                cout << elem.first << ' ' << elem.second<<endl;
            }
        }
    }
    const Value Search(const Key & key) {
        size_t pos = hash_func(key);
        for (auto elem: table[pos]) {
            if (elem.first == key) {
                return elem.second;
            }
        }
    }
    void Delete(const Key & key) {
        size_t pos = hash_func(key);
        int i = 0;
        auto it = table[pos].begin();
        cout << "Deleting"<< endl;
        for (auto elem: table[pos]) {
            cout << "Searching" << endl;
            if (elem.first == key) {
                cout << "Found:" << elem.first << endl;
                advance(it, i);
                table[pos].erase(it);
                break;
            }
            i +=1;
        }
    }
    const Value operator[](const Key& key){
        size_t pos = hash_func(key);
        for (auto elem: table[pos]) {
            if (elem.first == key) {
                return elem.second;
            }
        }
    }

};

int main ()
{
    HashTable <string, string> names;
    names.Insert("Q", "Alexey");
    names.Insert("Petrov", "Ivan");
    names.Insert("Smith", "Henry");
    names.Insert("Kuznetsov", "Semen");
    names.Output();
    names.Delete("Petrov");
    names.Output();
    cout << names["Q"] << endl;

}
